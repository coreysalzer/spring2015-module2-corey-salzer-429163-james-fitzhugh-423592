<?php
   session_start();
   if(!isset($_SESSION['username'])){
        header("Location: login.php");
   }
?>
<!DOCTYPE HTML>
<html>
   <head>
        <title> File Sharing Site </title>
   </head>
   <body>
        <form enctype="multipart/form-data" action="upload.php" method="POST">
            <p>
                <input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
                <label for="uploadfile_input">Choose a file to upload:</label> <input name="uploadedfile" type="file" id="uploadfile_input" />
            </p>
            <p>
               <input type="submit" value="Upload File" /><br><br>
            </p>

        </form>

        <form action="open.php" method="GET">
            <p>
                  File To Open : <input type="text" name="file" />
                  <button type="submit" value="Submit">Open</button>
            </p>

        </form>
        <?php
        echo "<h3> Files: </h3>";
        $username = $_SESSION['username'];
          if ($handle = opendir("/srv/uploads/$username")) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                 echo "$entry <br>";
                 }
            }
             closedir($handle);
         }
        ?>

        <form action="delete.php" method="GET">
            <p>
                  File To Delete : <input type="text" name="file" />
                  <button type="submit" value="Submit">Delete</button>
            </p>
        </form>

        <form action="logoff.php" method="GET">
            <p>
                  <button type="submit" value="Submit">Log Off</button>
            </p>
        </form>

   </body>
</html>
