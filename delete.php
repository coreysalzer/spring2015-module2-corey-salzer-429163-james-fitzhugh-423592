<?php
   session_start();
     if(isset($_GET['file'])){
        $filename = $_GET['file'];
                    if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
                    echo ("Invalid filename");
                    header("Location: fileSharing.php");
                    exit;
            }
             
            // Get the username and make sure that it is alphanumeric with limited other characters.
            // You shouldn't allow usernames with unusual characters anyway, but it's always best to perform a sanity check
            // since we will be concatenating the string to load files from the filesystem.
            $username = $_SESSION['username'];
            if( !preg_match('/^[\w_\-]+$/', $username) ){
                    echo ("Invalid username");
                    header("Location: login.php");
                    exit;
            }
             
            $full_path = sprintf("/srv/uploads/%s/%s", $username, $filename);
             
            // Now we need to get the MIME type (e.g., image/jpeg).  PHP provides a neat little interface to do this called finfo.
            $finfo = new finfo(FILEINFO_MIME_TYPE);
            $mime = $finfo->file($full_path);
             
             unlink($full_path);
             header("Location: fileSharing.php");
             
       }
?>